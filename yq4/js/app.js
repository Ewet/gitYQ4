	angular.module('myApp',['ionic'])
	/******************选项卡设置在底部的设置**************************/
	.config(function($ionicConfigProvider) {
		$ionicConfigProvider.platform.ios.tabs.style('standard'); 
		$ionicConfigProvider.platform.ios.tabs.position('bottom');
		$ionicConfigProvider.platform.android.tabs.style('standard');
		$ionicConfigProvider.platform.android.tabs.position('standard');
		
		$ionicConfigProvider.platform.ios.navBar.alignTitle('center'); 
		$ionicConfigProvider.platform.android.navBar.alignTitle('left');
		
		$ionicConfigProvider.platform.ios.backButton.previousTitleText('').icon('ion-ios-arrow-thin-left');
		$ionicConfigProvider.platform.android.backButton.previousTitleText('').icon('ion-android-arrow-back');
		
		$ionicConfigProvider.platform.ios.views.transition('ios'); 
		$ionicConfigProvider.platform.android.views.transition('android');
	})

	.config(function($stateProvider){
		$stateProvider
		
		.state('confirm_order',{
			templateUrl:'./confirm_order.html',
			controller:'confController'
		})
		.state('guide',{
			templateUrl:'./guide.html'
			
		})
		
		.state('home',{
			templateUrl:'./home.html',
			controller:'homeController'
		})
		
		.state('login',{
			templateUrl:'./login.html',
			controller:'logController'
		})
		.state('order',{
			templateUrl:'./order.html',
			controller:'orderController'
		})

		.state('pro_categories',{
			templateUrl:'./pro_categories.html',
			controller:'pro_cateController'
		})
		.state('pro_details',{
			templateUrl:'./pro_details.html',
			controller:'pro_detailsController'
		})
		.state('pro_list',{
			templateUrl:'./pro_list.html',
			controller:'pro_listController'
		})
		.state('register',{
			templateUrl:'./register.html',
			controller:'regController'
		})
		.state('search',{
			cache:"false",
			templateUrl:'./search.html',
			controller:'searchController'
		})
		.state('shop_car',{
			cache:"false",                    //设置页面点击后自动刷新
			templateUrl:'./shop_car.html',
			controller:'carController'
		})
		.state('user',{
			templateUrl:'./user.html',
			controller:'userController'
		})
		
	})
	
	//入口文件的引用
	.controller('stateCrl',function($scope,$state,$ionicHistory){
		$state.go('guide'),
	
		//返回函数
		$scope.back = function(){
			$ionicHistory.goBack();
		}
		function $(ele){
			return angular.element(document.querySelectorAll(ele))
		}
		
		$scope.home = function(){
			$state.go('home')
		}
		$scope.confirm_order = function(){
			$state.go('confirm_order')
		}
		$scope.login = function(){
			$state.go('login')
		}
		$scope.order = function(){
			$state.go('order')
		}
		$scope.pro_categories=function(){
			$state.go('pro_categories')
		}
		$scope.pro_details = function(){
			$state.go('pro_details')
		}
		$scope.pro_list = function(){
			$state.go('pro_list')
		}
		$scope.register = function(){
			$state.go('register')
		}
		$scope.search=function(){
			$state.go('search')
		}
		$scope.shop_car=function(){
			$state.go('shop_car')
		}
		$scope.user = function(){
			$state.go('user')
		}
		
	})
	
/***************guide.html********************/	
	.controller('gdController',function($scope){
		function $(ele){
		return angular.element(document.querySelectorAll(ele))
		}			
		$scope.slide = function($index){
			var cir = document.getElementsByTagName("span");
			if ($index == 3) {
				cir[$index].parentNode.style.display="none";
			} else{
				cir[$index].parentNode.style.display="block";
			}
		}
	})
	
	
/**********home.html*********/
		.controller('homeController',function($scope,$timeout,$ionicLoading){
			function $(ele){
				//以原生js的document对象的querySelector方法获取元素的id，括号内的使用方法同jquery一致，#代表id，
				    //该方法返回的是当前div的DOMElement对象，通过angular.element方法即可将即转化为一个jQuery对象，从而对其操作。
				return angular.element(document.querySelectorAll(ele))
			}
			$scope.inpu = function(){
				$timeout(function(){
					document.getElementById('text').focus();
				},100)
			}
			
			$scope.bigarr=[
				{id:1,img_src:'img/home_beg.png',text_h4:'家用厨具',text_p:'全场满199减100',bool:true},
				{id:2,img_src:'img/home_beg1.png',text_h4:'床上用品',text_p:'全场满199减100',bool:true},
				{id:3,img_src:'img/home_use.png',text_h4:'家用餐具',text_p:'全场满199减100',bool:false},
				{id:4,img_src:'img/home_min.png',text_h4:'装饰小件',text_p:'全场满199减100',bool:false},
				{id:5,img_src:'img/home_beg.png',text_h4:'家用厨具',text_p:'全场满199减100',bool:false},
				{id:6,img_src:'img/home_beg1.png',text_h4:'床上用品',text_p:'全场满199减100',bool:false},
				{id:7,img_src:'img/home_use.png',text_h4:'家用餐具',text_p:'全场满199减100',bool:false},
				{id:8,img_src:'img/home_min.png',text_h4:'装饰小件',text_p:'全场满199减100',bool:false},
				{id:9,img_src:'img/home_beg.png',text_h4:'家用厨具',text_p:'全场满199减100',bool:false},
				{id:10,img_src:'img/home_beg1.png',text_h4:'床上用品',text_p:'全场满199减100',bool:false},
				{id:11,img_src:'img/home_use.png',text_h4:'家用餐具',text_p:'全场满199减100',bool:false},
				{id:12,img_src:'img/home_min.png',text_h4:'装饰小件',text_p:'全场满199减100',bool:false},
			]
			$scope.startarr = []
		//分类筛选
			for(var i = 0;i<6;i++){
				$scope.startarr.push($scope.bigarr.shift())
			}
			$scope.moveBool=true;
			$scope.loadMove = function(){
				$timeout(function(){
					for(var y = 0;y<2;y++){
						var bigarrlen=$scope.bigarr.length
						if(bigarrlen==0){
							$(".produce>ul").append("<li class='like_li'>数据加载完毕...</li>")
							$scope.moveBool=false;
							return false;
						}else{
							$scope.startarr.push($scope.bigarr.shift())
						}
						$scope.$broadcast('scroll.infiniteScrollComplete')
					}
				},1000)
			}
		
			/*************************/
			$scope.Listarr=[
				{id:1,imgsrc:'img/home_member.png',text:'会员'},
				{id:2,imgsrc:'img/home_maney.png',text:'钱包'},
				{id:3,imgsrc:'img/home_play.png',text:'活动'},
				{id:4,imgsrc:'img/home_signup.png',text:'签到'},
			
			]

			/*************倒计时*****************/
			$scope.timer = function(){
				/*1小时30分钟倒计时*/
				var hour = 1;
				var minutes = 30;
				var seconds = 0;
				var milliseconds = 0;
				var interval = setInterval(start, 10);
				
				//初始化计时器
				//$(".timer span").text(j(hour) + ":" + j(minutes) + ":" + j(seconds) + ":" + j(milliseconds));
				document.getElementById("seconds").innerHTML=seconds;
				var num = document.getElementById("seconds").innerHTML=seconds;
			  	if(num<10){
			  		document.getElementById("seconds").innerHTML = "0" + num;
			  	}
			  	document.getElementById("points").innerHTML=minutes;
			  	var mini = document.getElementById("points").innerHTML=minutes;
			  	if(mini<10){
			  		document.getElementById("points").innerHTML= "0" + mini;
			  	}
			  	document.getElementById("house").innerHTML=hour;
			  	var house = document.getElementById("house").innerHTML=hour;
			  	if(house<10){
			  		document.getElementById("house").innerHTML= "0" + house;
			  	}
				//开始倒计时
				function start() {
					if(milliseconds == 0) {
						milliseconds=99;
						if(seconds == 0) {
							seconds = 59;
							if(minutes == 0){
								if(hour > 0){
									minutes = 59;
									hour--;
								}else {
									//时间到的情况
									hour = minutes = seconds = milliseconds = 0;
									document.getElementById("seconds").innerHTML=seconds;
									var num = document.getElementById("seconds").innerHTML=seconds;
								  	if(num<10){
								  		document.getElementById("seconds").innerHTML = "0" + num;
								  	}
								  	document.getElementById("points").innerHTML=minutes;
								  	var mini = document.getElementById("points").innerHTML=minutes;
								  	if(mini<10){
								  		document.getElementById("points").innerHTML= "0" + mini;
								  	}
								  	document.getElementById("house").innerHTML=hour;
								  	var house = document.getElementById("house").innerHTML=hour;
								  	if(house<10){
								  		document.getElementById("house").innerHTML= "0" + house;
								  	}
									//$(".timer span").text(j(hour) + ":" + j(minutes) + ":" + j(seconds) + ":" + j(milliseconds));
									clearInterval(interval);
									return false;
								}
							}else{
								minutes--;
							}
						}else{
							seconds--;
						}
					}else {
						milliseconds--;
					}
					//计时中
					document.getElementById("seconds").innerHTML=seconds;
					var num = document.getElementById("seconds").innerHTML=seconds;
				  	if(num<10){
				  		document.getElementById("seconds").innerHTML = "0" + num;
				  	}
				  	document.getElementById("points").innerHTML=minutes;
				  	var mini = document.getElementById("points").innerHTML=minutes;
				  	if(mini<10){
				  		document.getElementById("points").innerHTML= "0" + mini;
				  	}
				  	document.getElementById("house").innerHTML=hour;
				  	var house = document.getElementById("house").innerHTML=hour;
				  	if(house<10){
				  		document.getElementById("house").innerHTML= "0" + house;
				  	}
				}
			}
			$scope.timer();

		})
/**********user.html****************/
		.controller('userController',function($scope){
				$scope.olarr=[
					{id:1,imgsrc:'img/uesr/pic_02.png',text:'待付款'},
					{id:2,imgsrc:'img/uesr/pic_03.png',text:'待发货'},
					{id:3,imgsrc:'img/uesr/pic_04.png',text:'待收货'},
					{id:4,imgsrc:'img/uesr/pic_05.png',text:'待评价'},
					{id:5,imgsrc:'img/uesr/pic_06.png',text:'退货'},
				]
				
				$scope.ularr=[
					{id:1,imgsrc:'img/uesr/pic_07.png',text:'个人资料'},
					{id:2,imgsrc:'img/uesr/pic_08.png',text:'我的收藏'},
					{id:3,imgsrc:'img/uesr/pic_09.png',text:'我的足迹'},
					{id:4,imgsrc:'img/uesr/pic_10.png',text:'管理收货地址'},
					{id:5,imgsrc:'img/uesr/pic_11.png',text:'帮助与反馈'},
				]
			})

/***********pro_cateegories.html*******************/
		.controller('pro_cateController',function($scope,$timeout){
			function $(ele){
				return angular.element(document.querySelectorAll(ele))
			}
			//页面跳转之后另一个页面的搜索框自动获取焦点
			$scope.inpu = function(){
				$timeout(function(){
					document.getElementById('text').focus();
				},100)
			}
			
			$scope.click1 = function(){
				document.getElementById("kind").className="on";
				document.getElementById("brand").className="";
				document.getElementById("cate_list").style.display = "block";
				document.getElementById("brand_content").style.display = "none";
				
			}
			$scope.click2 = function(){
				document.getElementById("kind").className="";
				document.getElementById("brand").className="on";
				document.getElementById("cate_list").style.display = "none";
				document.getElementById("brand_content").style.display = "block";
			}
			
			
			
			
			$scope.olarr=[
			  	{id:1,imgsrc:'img/cate_list01.png',text_h4:'简约抱枕'},
			  	{id:2,imgsrc:'img/cate_list02.png',text_h4:'创意抱枕'},
			  	{id:3,imgsrc:'img/cate_list03.png',text_h4:'卡通抱枕'},
			  	{id:4,imgsrc:'img/cate_list04.png',text_h4:'欧式抱枕'},
			  	{id:5,imgsrc:'img/cate_list01.png',text_h4:'简约抱枕'},
			  	{id:6,imgsrc:'img/cate_list02.png',text_h4:'创意抱枕'},
			  	{id:7,imgsrc:'img/cate_list03.png',text_h4:'卡通抱枕'},
			  	{id:8,imgsrc:'img/cate_list04.png',text_h4:'欧式抱枕'},
			  	
			  ]
			 
			
		 
			
		})

/***********pro_list.html*************************************/
		.controller('pro_listController',function($scope,$timeout,$ionicScrollDelegate,$rootScope,$http){
			function $(ele){
				return angular.element(document.querySelectorAll(ele))
			}
			//页面跳转之后另一个页面的搜索框自动获取焦点
			$scope.inpu = function(){
				$timeout(function(){
					document.getElementById('text').focus();
				},100)
			}
			//头部选项卡功能
			$scope.proAll=function($index){
				if($index==0 || $index==undefined){
					$scope.order = 'id';
					$index=0;
					$(".list_top ul li").removeClass("on")
					$(".list_top ul li").eq($index).addClass("on");
						
				}else{
					$(".list_top ul li").removeClass("on")
					$(".list_top ul li").eq($index).addClass("on");
					
					if($index == 0){
						$scope.order = "id";
					}else if($index == 1){
						$scope.order = "sales";
					}else if($index == 2){
						$scope.order = "price";
					}else if($index == 3){
						$scope.order = "screen";
					}
				}
			}
			$scope.proAll();
			
			$scope.obj = {
				"pro_shop":[
					{"id":1,"img" : "img/list_01.png","screen":"451","sales":500,"title":"A0240 办公室抱枕","price":"120.00","img_car":"img/pro_details_car.png"},
					{"id":2,"img" : "img/list_02.png","screen":"215","sales":300,"title":"A0240 办公室抱枕","price":"110.00","img_car":"img/pro_details_car.png"},
					{"id":3,"img" : "img/list_03.png","screen":"458","sales":600,"title":"A0240 办公室抱枕","price":"160.00","img_car":"img/pro_details_car.png"},
					{"id":4,"img" : "img/list_04.png","screen":"362","sales":100,"title":"A0240 办公室抱枕","price":"154.00","img_car":"img/pro_details_car.png"},
					{"id":5,"img" : "img/list_05.jpg","screen":"542","sales":100,"title":"A0240 办公室抱枕","price":"415.00","img_car":"img/pro_details_car.png"},
					{"id":6,"img" : "img/list_01.png","screen":"125","sales":500,"title":"A0240 办公室抱枕","price":"120.00","img_car":"img/pro_details_car.png"},
					{"id":7,"img" : "img/list_02.png","screen":"154","sales":300,"title":"A0240 办公室抱枕","price":"110.00","img_car":"img/pro_details_car.png"}
				]
			}
			
			//页面刷新效果
			$scope.more = true;
			//备用数组对象，用于刷新加载
			$scope.proArr = [
				{"id":1,"img" : "img/list_03.png","screen":"458","sales":600,"title":"A0240 办公室抱枕","price":"160.00","img_car":"img/pro_details_car.png"},
				{"id":2,"img" : "img/list_04.png","screen":"362","sales":100,"title":"A0240 办公室抱枕","price":"154.00","img_car":"img/pro_details_car.png"},
				{"id":3,"img" : "img/list_05.jpg","screen":"542","sales":100,"title":"A0240 办公室抱枕","price":"415.00","img_car":"img/pro_details_car.png"},
				{"id":4,"img" : "img/list_01.png","screen":"125","sales":500,"title":"A0240 办公室抱枕","price":"120.00","img_car":"img/pro_details_car.png"},
				{"id":5,"img" : "img/list_02.png","screen":"154","sales":300,"title":"A0240 办公室抱枕","price":"110.00","img_car":"img/pro_details_car.png"},
				{"id":6,"img" : "img/list_03.png","screen":"451","sales":600,"title":"A0240 办公室抱枕","price":"160.00","img_car":"img/pro_details_car.png"},
				{"id":7,"img" : "img/list_01.png","screen":"125","sales":500,"title":"A0240 办公室抱枕","price":"120.00","img_car":"img/pro_details_car.png"},
				{"id":8,"img" : "img/list_04.png","screen":"369","sales":100,"title":"A0240 办公室抱枕","price":"154.00","img_car":"img/pro_details_car.png"}
			]
			//刷新加载函数
			$scope.loadMore = function(){
				$timeout(function(){
					for(var i=0; i <4; i++){
						if( $scope.proArr.length == 0 ){
							$scope.more = false;
							$('.list_content').append('<div class="loadend-tip" style="text-align: center;">数据加载完毕</div>');
							break;
						} else{
							//把proAA数组数据push进proList数组里面
							$scope.obj["pro_shop"].push( $scope.proArr.shift() );
						}
						//发送刷新事件
						$scope.$broadcast('scroll.infiniteScrollComplete');
					}
				},1500)
			}	
			
			
			//加入购物车
			//页面加载之后先进行数组的创建
			var prolacal = localStorage.getItem('Myprolist'),
			 	Myprolist = [],
			 	len,
			 	proLcalJson =JSON.parse(prolacal);
			if(proLcalJson !=null ){
				len = proLcalJson.length;;
				for(i=0;i<len;i++){
					Myprolist.push(proLcalJson[i])
				}
			}
			//加入购物车函数	
		 	$scope.addshopcar=function(){
		 		var pro_title = $(".list_order li h3").html(),
			 		pro_num=1,
			 		pro_img = $(".list_order li img.produce").attr('src'),
			 		pro_price = $(".list_order li span").html().replace("￥",'');
			 	console.log(pro_title)	
			 	console.log(pro_img)
			 	console.log(pro_price)
		 		for(var x=0;x<Myprolist.length;x++){
		 			if(pro_title == Myprolist[x].title){
		 				Myprolist[x].num = parseInt(Myprolist[x].num)+parseInt(pro_num)
		 				localStorage.setItem("Myprolist",JSON.stringify(Myprolist));
		 				console.log(localStorage.getItem("Myprolist"))
		 				return false;
		 			}
		 		}
		 		localDate(pro_title,pro_num,pro_img,pro_price)
		 
		 	
			 	function localDate(title,num,img,price){
			 		var product = {
			 			title:title,
			 			num:num,
			 			img:img,
			 			price:price,
			 			checked:false
			 		}
			 		Myprolist.push(product)
			 		localStorage.setItem("Myprolist",JSON.stringify(Myprolist))
			 		console.log(localStorage.getItem("Myprolist"))
			 	}
		 	}
			
		})
/**********confirm_order.html****************************************/
			.controller('confController',function($scope,$ionicPopup){
				function $(ele){
					return angular.element(document.querySelectorAll(ele))
				}
				
				/************加减输入*************/
				var input_val=document.getElementById("input_val").value
				
				 document.getElementById("input_val").onfocus=function(){
				 	input_val=document.getElementById("input_val").value
				 }
				 document.getElementById("input_val").onblur=function(){
				 	input_val_=document.getElementById("input_val").value
				 	if(parseInt(input_val_)==input_val_ && input_val_>=1 && input_val_<=99){
				 		document.getElementById("input_val").value=input_val_
				 	}else if(input_val_>99){
				 		input_val_=99
				 		document.getElementById("input_val").value=input_val_
				 	}else if(input_val_<1){
				 		input_val_=1
				 		document.getElementById("input_val").value=input_val_
				 	}else{
				 		document.getElementById("input_val").value=input_val
				 	}
				 	num()
				 	money()
				 }
				 $scope.miuns=function(){
				 	var input=document.getElementById("input_val").value
				 	if(input>1 && input<=99){
				 		input--
				 	}
				 	document.getElementById("input_val").value=input
				 	num()
				 	money()
				 }
					var reduce = document.getElementById("reduce");
				 	reduce.onclick = function(){
				 		reduce.style.background = "#F2F2F2";
				 		setTimeout(function(){
				 			reduce.style.background = "transparent";
				 		},100)
				 	}
				 $scope.adds=function(){
				 	var input=document.getElementById("input_val").value
				 	if((input>=1) && (input<99)){
				 		input++
				 	}
				 	document.getElementById("input_val").value=input
				 	num()
				 	money()
				 }
				  	var add = document.getElementById("add");
				 	add.onclick = function(){
				 		add.style.background = "#F2F2F2";
				 		setTimeout(function(){
				 			add.style.background = "transparent";
				 		},100)
				 	}
				 	
				/*************商品件数****************/ 	
				 	var num = function(){
				 		var input_val=document.getElementById("input_val").value;
				 		document.getElementById("number").innerHTML = input_val;
				 	}
				/*************小计金额*********************/ 	
					var money = function(){
						var cont = document.getElementById("number").innerHTML;
						var total = cont*998;
						document.getElementById("total").innerHTML = "¥ " +total.toFixed(2);
						document.getElementById("all_total").innerHTML = "¥"+total.toFixed(2);
						document.getElementById("need_pay").innerHTML = "¥"+total.toFixed(2);
					}
					
				/**************提交订单按钮*****************/	
					$scope.submit = function(){
						var payage = document.getElementById("payage")
						payage.style.display = "block";
					}
				/***************确认支付按钮*******************/	
				
				$scope.cf_pay = function(){
					if(confirm("请，您确定要支付吗")){
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "block";
						payfailure.style.display = "none";
					}else{
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "none";
						payfailure.style.display = "block";
					}
				}
				/***************继续购物按钮******************/
					$scope.go_more = function(){
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "none";
						payfailure.style.display = "none";
						
					}
					$scope.go_back = function(){
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "none";
						payfailure.style.display = "none";
						
					}
					$scope.fai_back = function(){
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "none";
						payfailure.style.display = "none";
						
					}
					$scope.order_detail = function(){
						var payage = document.getElementById("payage")
						var paysuccess = document.getElementById("paysuccess")
						var payfailure = document.getElementById("payfailure")
						payage.style.display = "none";
						paysuccess.style.display = "none";
						payfailure.style.display = "none";
						
					}
				
			})
/***********pay_success.html************************************/
			.controller('pay_scController',function($scope){
				
			})
/***********pay_failure.html************************************/
			.controller('pay_faController',function($scope){
				
			})
/***********login.html***********************************/
			.controller('logController',function($scope,$http,$state,$ionicPopup){
				function $(ele){
					return angular.element(document.querySelectorAll(ele))
				}
				$scope.submitForm = function(form,isValid) {
					var ph = loginForm.account.value;
					
					var pwd = loginForm.pwd.value;
					console.log(pwd.length)
					if(ph == ""){
						$ionicPopup.alert({
							title: "手机号码不能为空"
						})
					}else if(pwd == ""){
						$ionicPopup.alert({
							title: "密码不能为空"
						})
					}else if(ph.length != 11){
						$ionicPopup.alert({
							title: "手机号码输入有误"
						})
					}else if(pwd.length != 6){
						$ionicPopup.alert({
							title: "请输入六位数字的密码"
						})
					}
					
			        if(isValid) {
			        	
			        	$http.get("http://25798_t69X525.php.100dan.cn/js/user.json").success(function(data) {
							for(var i in data){
								console.log(14)
								console.log(data[i].phoneNumber)
								
								if(data[i].phoneNumber == loginForm.account.value && data[i].password == loginForm.pwd.value) {
									console.log("登录成功")
						        	$state.go("home")
						        	return true;
								}
							}
							$ionicPopup.alert({
								title: "输入错误或未注册"
							})
						})
			
			        }
			   	};
							
			})
/********************注册页需要的服务*******************************/
		.config(function($httpProvider,$ionicConfigProvider){
				$httpProvider.defaults.transformRequest = function(obj){
					var str = [];
					for(var p in obj){
						str.push(encodeURIComponent(p) +"="+ encodeURIComponent(obj[p]));
					}
					return str.join("&");
				}
				$httpProvider.defaults.headers.post = {
					'Content-Type':'application/x-www-form-urlencoded'		
				}
				/*取消视图切换动画*/
				$ionicConfigProvider.views.transition('none');
	
		})
		.config(['$httpProvider', function($httpProvider) {

		    if (!$httpProvider.defaults.headers.get) {
		        $httpProvider.defaults.headers.get = {};    
		    }	
		    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
		    // extra
		    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
		    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
		}])
/***********register.html***********************************/
			.controller('regController',function($scope, $state, $http,$ionicPopup,$interval){
				function $(ele){
					return angular.element(document.querySelectorAll(ele))
				}
				
				$scope.submitForm = function(form,isValid) {
					
					
			        if (!isValid) {
			        	if(form.phonenumber.$pristine) {
			        		$ionicPopup.alert({title: "请输入手机号码"})
			        		return false;
			        	}
				        if(form.pwd.$invalid) {
				        	$ionicPopup.alert({title: "请输入至少六位数的密码"})
				        	return false;
				        }
			        	if(form.captcha.$invalid) {
			        		$ionicPopup.alert({title: "请输入验证码"})
			        		return false;
			        	}
			        	
			        }else {
						$http.get("http://25798_t69X525.php.100dan.cn/js/user.json").success(function(data) {
						
							for(var i in data){
								if(data[i].phoneNumber == registerForm.phonenumber.value) {
				        			$ionicPopup.alert({title: "该手机号码已被注册"})
									return false;
								}
							}
							$scope.user = {
								phoneNumber: registerForm.phonenumber.value,
								password: registerForm.pwd.value
							}
							$http({
								method:"POST",
								url:"http://25798_t69X525.php.100dan.cn/js/index.php",
								data:$scope.user
							}).success(function(data){
								$scope.showAlert = function(){
									var alertPopup = $ionicPopup.alert({
										title: "注册成功"
									});
									alertPopup.then(function(res){
										$state.go("login");
									})
								}
								$scope.showAlert();
							})
						})

			        }
			    };
				
	
				//获取验证码
				$scope.codeText = "验证";
				var source = 3;
				$scope.codeEvent = function(){
					$scope.codeText = source+"s后获取"
					
					var time = $interval(function(){
						if(source==0){
							$scope.codeText = "重获"
							
							$interval.cancel(time)
							source = 3;
						}else{
							source -- ;
							$scope.codeText = source+"s后获取"
						}
					},1000)
				}
				
			})
/**********shop_car.html*************************/
			.controller('carController',function($scope,$ionicPopup,$state){
				function $(ele){
				return angular.element(document.querySelectorAll(ele))
				}
				
				//所选商品数量
				$scope.num=0;
				$scope.numb = function(){
					var a = 0;
					for(var i=0;i<$scope.Myprolist.length;i++){
						
						if($scope.Myprolist[i].checked){
							a+=1
						}
					}
					$scope.num = a;
				}
				
				$scope.shoplist=[
					{id:1,imgsrc:"img/home_beg.png"},
					{id:2,imgsrc:"img/home_beg1.png"},
					{id:3,imgsrc:"img/home_min.png"},
					{id:4,imgsrc:"img/home_use.png"},
				]
				//往购物车中插入值
				var firstfun = function(){
					$scope.Myprolist = JSON.parse(localStorage.getItem("Myprolist"))
					var list = localStorage.getItem("Myprolist"),
					listJson = JSON.parse(list);
					localStorage.setItem("Myprolist",JSON.stringify(listJson))
					//改变输入框的值
					$scope.inputVal = []
					if($scope.Myprolist!=null || $scope.Myprolist!=undefined){
						for(var i=0;i<$scope.Myprolist.length;i++){
							$scope.inputVal.push(parseInt($scope.Myprolist[i].num))
						}		
					}
				}
				firstfun()
				
				
				//初始化总金额				
				$scope.initVal ={
					total:0,
				}
				
				//单选功能
				$scope.check = function(bool,index){
					if(bool){
						setCheck(bool)
						getTotal()
					}else{
						setCheck(bool)
						getTotal()
					}
						var list = localStorage.getItem("Myprolist"),
							listJson = JSON.parse(list);
							listJson[index].checked= bool
							localStorage.setItem("Myprolist",JSON.stringify(listJson))
							$scope.Myprolist = JSON.parse(localStorage.getItem("Myprolist"))

					$scope.numb();
					
				}
				//全选功能函数
				$scope.checkedAll = function(bool){
					
					if(bool){
						setCheckAll(bool)
					}else{
						setCheckAll(bool)
					}
					
				}
				//计算总价
				function getTotal(){
					$scope.initVal.total=0
					if($scope.Myprolist!=null || $scope.Myprolist!=undefined){
						for(var i=0;i<$scope.Myprolist.length;i++){
							if($scope.Myprolist[i].checked){
								$scope.initVal.total += $scope.Myprolist[i].price* $scope.Myprolist[i].num;	
							}
						}	
						
					}
				}
				//判断全选功能
				function setCheck(bool){
					if($scope.Myprolist!=null || $scope.Myprolist!=undefined){
						for(var i=0;i<$scope.Myprolist.length;i++){
							if($scope.Myprolist[i].checked){
								$scope.checkAll = true
							}else{
								$scope.checkAll = false
								break
							}
						}	
					}
				}
				
				//改变本地存储中的选中样式
				function setCheckAll(bool,index){
					
					for(var i=0;i<$scope.Myprolist.length;i++){
						$scope.Myprolist[i].checked = bool
						
					}
					$scope.numb();
					getTotal()
				}
				
				//减少
				$scope.miuns = function(index){
					if($scope.Myprolist[index].num>1){
						$scope.Myprolist[index].num --
//						//让本地中的num值减少然后用于计算总价
						var list = localStorage.getItem("Myprolist"),
							listJson = JSON.parse(list);
							listJson[index].num= $scope.Myprolist[index].num
							localStorage.setItem("Myprolist",JSON.stringify(listJson))
							$scope.Myprolist = JSON.parse(localStorage.getItem("Myprolist"))
						firstfun()
						getTotal()
						$scope.numb();
					}
				}
				//增加
				$scope.add = function(index){
					if($scope.Myprolist[index].num<99){
						$scope.Myprolist[index].num ++
						var list = localStorage.getItem("Myprolist"),
							listJson = JSON.parse(list);
							listJson[index].num= $scope.Myprolist[index].num
							localStorage.setItem("Myprolist",JSON.stringify(listJson))
							$scope.Myprolist = JSON.parse(localStorage.getItem("Myprolist"))
						firstfun()
						getTotal()
						$scope.numb()
					}else{
						$ionicPopup.alert({
							title:"已达商品购买最大量",
							okText:'确定',
							okType:'button-positive',
						})
					}
				}
				
				//输入框
				$scope.blurText = function(index){
					var a = $(".right_up input[type=text]").val();
					if(isNaN(a)){
						$scope.Myprolist[index].num=1
					}else if(a>99){
						$scope.Myprolist[index].num=99
					}else if(a<=0){
						$scope.Myprolist[index].num=1
					
					}else{
						$scope.Myprolist[index].num=a
					}
					var list = localStorage.getItem("Myprolist"),
							listJson = JSON.parse(list);
							listJson[index].num= $scope.Myprolist[index].num
							localStorage.setItem("Myprolist",JSON.stringify(listJson))
							$scope.Myprolist = JSON.parse(localStorage.getItem("Myprolist"))
						firstfun()
					getTotal()
				}
				//结算商品
				$scope.goCom = function(){
						if($scope.Myprolist!=null || $scope.Myprolist!=undefined){
							for(var i=0;i<$scope.Myprolist.length;i++){
								if($scope.Myprolist[i].checked){
									$state.go('confirm_order')
								}else{
									$ionicPopup.alert({
										title:"请选择商品",
										okText:'确定',
										okType:'button-positive',
									})
								}
							}
						}else{
							$ionicPopup.alert({
								title:"请选择商品",
								okText:'确定',
								okType:'button-positive',
							})
						}
					
				}
				//删除商品
				$scope.del = function(index){
					
//					提示框
					var confirmPopup = $ionicPopup.confirm({
						title:"确认删除选中商品",
						okText:'确定',
						okType:'button-positive',
						cancelText:'取消',
						cancelType:'button-assertive'
					})
					confirmPopup.then(function(rel){
						if(rel){
							$scope.Myprolistjsn = JSON.parse(localStorage.getItem("Myprolist"))
							var proArr=[],
								newproArr=[],
								a=0;
							if($scope.Myprolistjsn!=null || $scope.Myprolistjsn!=undefined){
								//如果本地存储不为空，把它放在一个新的数组里面
								console.log(1)
								for(var i=0;i<$scope.Myprolistjsn.length;i++){
									proArr.push($scope.Myprolistjsn[i])
								}		
								$(".go_bay input[type=checkbox]").prop("checked",false)
								
							}
							for(var i=0;i<proArr.length;i++){
								if($scope.Myprolist[i].checked==false){
									//循环遍历，把未选中的放在一个新的数组里面
									newproArr.push($scope.Myprolist[i])
									$scope.checkAll = true;
									
								}else{
									console.log(4)
									//如果有选中的，算出选中的的价格，用原来重甲减去选中的价格，变成新的总价，在进行赋值
											a+=1;
									  var  old_mon = parseInt(proArr[i].price)*parseInt(proArr[i].num),
										   all_mon = $scope.initVal.total,
										   new_mon = all_mon - old_mon;
										   $scope.initVal.total = new_mon;
										  // $(".all_pri p>span").text("￥"+new_mon+".00")
									$(".go_bay input[type=checkbox]").prop("checked",false)
									$scope.num = 0;
								}
								//把未选择的存进本地
								localStorage.setItem("Myprolist",JSON.stringify(newproArr))
								//循环拿出来进行挑选选中的
								localStorage.getItem("Myprolist")
							}
							//定义一个变量来判断是否选中，未选中的话使用弹出框提示
							if(a==0){
								$ionicPopup.alert({
									title:"请选择商品",
									okText:"确定"
								})
							}else{
								$scope.Myprolist.splice(index,1)
								$scope.numb();
							}
						}
					})
					$scope.check()
				}
				
			})

/**********search.html**********************/
			.controller('searchController',function($scope,$timeout){
				$timeout(function(){
					document.getElementById('text').focus();
				},100)
				
				
				$scope.search_list=[
					{"id":"1","text":"家居抱枕"},
					{"id":"2","text":"家用厨具"},
					{"id":"3","text":"家电"},
					{"id":"4","text":"美食"},
					{"id":"5","text":"地板纺织"},
					{"id":"6","text":"卫浴美体"},
					{"id":"7","text":"灯饰类"},
					{"id":"8","text":"餐茶玻璃"},
					{"id":"9","text":"饰品花艺"},
					{"id":"10","text":"层架收纳"},
					{"id":"11","text":"家居抱枕"}
				]
				
				
			})
/***********pro_details.html********************************************/
		.controller('pro_detailsController',function($scope,$rootScope,$http,$timeout,$ionicLoading,$window){
			function $(ele){
				return angular.element(document.querySelectorAll(ele))
				}
			//页面加载之后先进行数组的创建
			var prolacal = localStorage.getItem('Myprolist'),
			 	Myprolist = [],
			 	len,
			 	proLcalJson =JSON.parse(prolacal);
			if(proLcalJson !=null ){
				len = proLcalJson.length;;
				for(i=0;i<len;i++){
					Myprolist.push(proLcalJson[i])
				}
			}
			//加入购物车函数	
		 	$scope.addshopcar=function(){
		 		var pro_title = $(".pro_massege .left h3").html(),
		 		pro_num=1,
		 		pro_img = $(".img_slide img").attr('src'),
		 		pro_price = $(".pro_massege .left b").html().replace("￥",'');
		 		for(var x=0;x<Myprolist.length;x++){
		 			if(pro_title == Myprolist[x].title){
		 				Myprolist[x].num = parseInt(Myprolist[x].num)+parseInt(pro_num)
		 				localStorage.setItem("Myprolist",JSON.stringify(Myprolist));
		 				console.log(localStorage.getItem("Myprolist"))
		 				return false;
		 			}
		 		}
		 		
		 		localDate(pro_title,pro_num,pro_img,pro_price)
		 	
			 	function localDate(title,num,img,price){
			 		var product = {
			 			title:title,
			 			num:num,
			 			img:img,
			 			price:price,
			 			checked:false
			 		}
			 		Myprolist.push(product)
			 		localStorage.setItem("Myprolist",JSON.stringify(Myprolist))
			 		console.log(localStorage.getItem("Myprolist"))
			 	}
		 	}
		 	
		})
/***********order.html*******************************************/
		.controller('orderController',function($scope,$ionicPopup){
				
			
				$scope.Allarr=[
					{id:1,h1_span1:"订单号：1009456789",h1_span2:"待付款",imgsrc:"img/confirm_or_pic.png",right_h3:"米乐 加厚面料 亚麻布艺抱枕欧式垫办公室抱枕靠垫靠枕",p1_small:"¥ ",p1_b:"998-1998",p1_span:"卖家促销",right_p2:"价格",right_p2_span:"￥1099-2099",right_p2_i:"x1",
					total_p:"共1件商品",total_p_span:"合计：",total_b:"¥998.00",total_small:"（含运费：￥00.00）",timeway_span:"下单时间：2016.12.21",timeway_bt1:"付款",timeway_bt2:"取消订单"},
					
					{id:2,h1_span1:"订单号：1009456789",h1_span2:"待付款",imgsrc:"img/confirm_or_pic.png",right_h3:"米乐 加厚面料 亚麻布艺抱枕欧式垫办公室抱枕靠垫靠枕",p1_small:"¥ ",p1_b:"998-1998",p1_span:"卖家促销",right_p2:"价格",right_p2_span:"￥1099-2099",right_p2_i:"x1",
					total_p:"共1件商品",total_p_span:"合计：",total_b:"¥998.00",total_small:"（含运费：￥00.00）",timeway_span:"下单时间：2016.12.21",timeway_bt1:"付款",timeway_bt2:"取消订单"},
					
					{id:3,h1_span1:"订单号：1009456789",h1_span2:"待付款",imgsrc:"img/confirm_or_pic.png",right_h3:"米乐 加厚面料 亚麻布艺抱枕欧式垫办公室抱枕靠垫靠枕",p1_small:"¥ ",p1_b:"998-1998",p1_span:"卖家促销",right_p2:"价格",right_p2_span:"￥1099-2099",right_p2_i:"x1",
					total_p:"共1件商品",total_p_span:"合计：",total_b:"¥998.00",total_small:"（含运费：￥00.00）",timeway_span:"下单时间：2016.12.21",timeway_bt1:"付款",timeway_bt2:"取消订单"},
				]
				function $(ele){
					return angular.element(document.querySelectorAll(ele))
				}
				
				/***********取消订单按钮*************/
				
				$scope.cod = function($index){
					console.log($index)
					document.getElementsByClassName("order_list")[$index].style.display = "none"
					
				}
			
			

		})

















